#!/usr/bin/env python
import argparse
from pyparsing import   Word, alphas, nums,\
                        alphanums, quotedString, Keyword,\
                        Literal, OneOrMore, Regex,\
                        LineEnd, LineStart

package_list = dict()
state_list = dict()
rconfig_list = dict()
class EvaluatePackage(object):
    def __init__(self, tokens):
        self.tokens = tokens
        if tokens[2] not in package_list:
            package_list[tokens[2]]=dict()
        package_list[tokens[2]][tokens[4]] = tokens[1]

    def __str__(self):
        return " ".join(self.tokens)

class EvaluateRconfigNamelist(object):
    def __init__(self, tokens):

        filler=[None]*(11 if 11 > len(tokens) else len(tokens))
        self.tokens = filler
        self.tokens[:len(tokens)] = tokens

        name,nml,value,desc =   self.tokens[2],\
                                self.tokens[4],\
                                self.tokens[6],\
                                self.tokens[9]

        d = rconfig_list.setdefault(nml,dict())
        if name not in d:
            d[name] = dict(value=value,desc=desc)
        else:
            raise Exception("Found duplicate key for {0}".format(self.tokens.join(" ")))

class EvaluateState(object):
    def __init__(self, tokens):
        self.tokens = tokens
        if tokens[2] not in state_list:
            state_list[tokens[2]] = self.construct(tokens)

    def construct(self, tokens):
        l=len(tokens)
        names=["Table","Type","Sym","Dims","Use","NumTLev","Stagger","IO","DNAME","DESCRIP","UNITS"]
        d=dict(zip(names[:l],tokens[:l]))
        use=["Type","DESCRIP"]
        return dict(zip(use,[d[v] for v in use if v in d]))

def printme(c):
    pass#print c

class RegistryParser(object):
    def __init__(self):
        floatNumber = Regex(r'[-]?\d+(\.\d*)?([eE][-+]?\d+)?') | Word(nums +"-.")
        variable = Word(alphanums + "_:,-.")
        entry = Word(alphanums+"_-,.") | quotedString()

        ##Look for lines starting with keywords and a particular pattern.
        ##Right now we only look at particular lines, like package
        package =   Keyword("package") + \
                    variable + variable + \
                    Literal("==") + \
                    floatNumber + variable + variable

        rconfig_namelist = Keyword("rconfig") + variable + variable + Literal("namelist,")+OneOrMore(entry)
        rconfig_derived = Keyword("rconfig") + variable + variable + Literal("derived")+OneOrMore(entry)
        state = Keyword("state") + OneOrMore(entry)
        halo = Keyword("halo") + OneOrMore(entry)
        include = Keyword("include") + variable
        comment = Literal("#")
        xpose = Keyword("xpose")
        period = Keyword("period")
        typedef = Keyword("typedef")
        odd =   Keyword("i1") | Literal("i0") | Literal("ifdef") | \
                Literal("endif") | Literal("ifndef") | Literal("irh") | \
                Literal("rus") | Literal("nce") | Literal("dimspec")| Literal("moist")

        package.setParseAction(EvaluatePackage)
        state.setParseAction(EvaluateState)
        rconfig_namelist.setParseAction(EvaluateRconfigNamelist)
        blankline = OneOrMore(LineEnd())

        self.parser =   package | rconfig_namelist | rconfig_derived | state | \
                         include | comment | halo | \
                         xpose | period | typedef | \
                         odd | blankline

        #self.parser.ignore(blankline)
        #parser2 = package | rconfig
        #self.parser.setDebug()

        self.lines_attempted=0
        self.lines_failed=0

        self.unparsed_keywords=[]

    def parseString(self, line):
        if len(line):
            try:
                self.lines_attempted+=1
                self.parser.parseString(line)
            except Exception as e:
                self.lines_failed+=1
                print e
                entries=line.split()
                if len(entries):
                    self.unparsed_keywords.append(entries[0])

    def parse(self,filename):
        for line in open(filename):
            self.parseString(line)

def main(filename, output):
    d = RegistryParser()
    d.parse(filename)
    print d.lines_failed*100.0/d.lines_attempted
    #print d.unparsed_keywords

    with open(output,'w') as handle:
        import json
        json.dump(   dict(package=package_list,state=state_list,rconfig=rconfig_list),
                     handle,
                     indent=True)

if __name__=="__main__":
    import sys
    parser = argparse.ArgumentParser(description="Parse Registry file")
    parser.add_argument("registry",   type=str, help="Registry file to generate dictionary")
    parser.add_argument("dictionary", type=str, help="Dictionary filename to save")
    args = parser.parse_args()
    main(args.registry, args.dictionary)

