#!/usr/bin/env python
import f90nml
import json
import sys
import os
import argparse
from collections import OrderedDict
def decode_namelist(nml, decoder,myfilter=None):
    if myfilter is None:
        myfilter=lambda x:  True
    lines =[]
    for key,value in sorted(nml.items()):
        value_int, location = value
        if not myfilter(key):
            continue
        value=str(value_int)
        result=value
        if key in decoder["package"]:
            result = 'none'
            if value in decoder["package"][key]:
                result = "{0} #{1}".format(value,decoder["package"][key][value])
            else:
                result=value
        lines.append("\t{0} = {1} #({2})".format(key,result,location))
    return lines

def thesame(a,b):
    if a==False:
        a=".false."
    if a==True:
        a=".true."
    if b==False:
        b=".false."
    if b==True:
        b=".true."
    
    if a==b:
        return True
    

def parse_namelist(nml,decoder,namelist,filter=None):
    if namelist not in decoder["rconfig"]:
        print "Namelist {0} not found in Registry".format(namelist)
        return None

    res=dict()
    for k,v in decoder["rconfig"][namelist].items():
        res[k]=(v["value"],'R')
    if namelist in nml:
        for k,v in nml[namelist].items():
            if k in res:
                if thesame(res[k][0],v):
                    res[k]=(v,"RN")
                else:
                    res[k] = (v,'N')

    return decode_namelist(res,decoder,myfilter=filter)

def main(filename, decoder, namelist=None,filter=None):
    if decoder is None:
        decoder = os.path.join(os.path.dirname(os.path.realpath(__file__)), "default_decoder.json")

    nml = f90nml.read(filename)
    decoder = json.load(open(decoder))

    if namelist is not None:
        if any([a == "all"  for a in namelist]):
            namelist = sorted(decoder["rconfig"].keys())
        for s in namelist:
            print "#{0}".format(s)
            lines = parse_namelist(nml,decoder,s,filter=filter)
            for l in lines:
                print l

def diff(filenames, decoder, namelist=None, filter=None):
    if decoder is None:
        decoder = os.path.join(os.path.dirname(os.path.realpath(__file__)), "default_decoder.json")
    decoder = json.load(open(decoder))
    #eww recursive

    nmls=OrderedDict()
    for filename in filenames:
        nml = f90nml.read(filename)
        
        nmls[filename] = dict()
        nmls[filename]['_all']=list()
        if namelist is not None:
            if any([a == "all"  for a in namelist]):
                namelist = sorted(decoder["rconfig"].keys())
            for s in namelist:
                n=parse_namelist(nml,decoder,s,filter=filter)
                nmls[filename][s] = n
                nmls[filename]['_all'].extend(n)
    
    #common values to all
    common_list = dict()
    for k in nmls.keys():
        if k=="_all":
            continue
        for s in nmls[k].keys(): #namelists
            if s not in common_list:
                common_list[s] = set(nmls[k][s])
            else:
                common_list[s] = common_list[s].intersection(nmls[k][s])
    
    def exceptfor(l,k):
        return filter(lambda x: x!=k,l)

    #print filenames
    diff_against_common = dict()
    for mykey in nmls.keys():
        diff_against_common[mykey] = dict()
        for s in nmls[mykey].keys():
            diff_against_common[mykey] = set(nmls[mykey][s]).difference(common_list[s])


    for s in sorted(common_list.keys()):
        if s=="_all":
            continue
        print "#{0}".format(s)
        print "\n".join(sorted(common_list[s]))

    for mykey in sorted(nmls.keys()):
        print "difference of {0} from common".format(mykey)
        for s in sorted(nmls[mykey].keys()):
            if s=="_all":
                continue
            diffs = set(nmls[mykey][s]).difference(common_list[s])
            print "#{0}".format(s)
            print "\n".join(sorted(diffs))

    #html difference
    #print the common list
    html=open("diff.html",'w')
    html.write("""
<html>
  <head>
  </head>
  <body>
""")
    slist = sorted(common_list.keys())
    for key in slist:
        html.write("<h1>{0}</h1>\n".format(key))
        html.write("<ul>\n")
        for k in sorted(common_list[key]):
            html.write("<li>{0}</li>\n".format(k))
        html.write("</ul>\n")
        #now make a table

        def all_union(nmls, key, common_list):
            s=set()
            for fname in nmls.keys():
                s=s.union(set(nmls[fname][key]).difference(common_list[key]))
            s=sorted(set([a.split("=")[0][1:-1] for a in s]))
            return s
        all_keys = all_union(nmls,key, common_list)
        html.write("<table>\n")
        html.write("<tr><td>Model</td><td>{0}</td></tr>".format("</td><td>".join(all_keys)))
        for fname in nmls.keys():
            di=dict((a.split("=")[0][1:-1],a.split("=")[1].strip()) for a in nmls[fname][key])
            d=[di[a] if a in di else "--" for a in all_keys]
#            d=[]
#            
#            for a in all_keys:
#                if a in nmls[fname][key]:
#                    d.append(nmls[fname][ke)
#                else:
                    
            html.write("<tr><td><a href=\"{0}/index.html\">{1}</a></td><td>{2}</td></tr>".format(fname.split("/")[0],fname,"</td><td>".join(d)))
        
        html.write("</table>\n")
    html.write("</body></html>")
    html.close()


def csv(s):
    return s.split(",")

if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Decode namelist file")
    parser.add_argument("filename",   type=str, nargs="+", help="namelist file to generate dictionary")
    parser.add_argument("--dictionary", "-d", dest='dictionary',type=str, help="Dictionary filename to save")
    parser.add_argument("--namelist", "-n", dest='namelist',type=csv, help="Dictionary filename to save",default=["physics"])
    parser.add_argument("--substring", "-s", dest='substring',type=str, help="contains substring",default=None)
    parser.add_argument("--diff",action="store_true")
    args = parser.parse_args()

    filter=None
    if args.substring is not None:
        filter = lambda x: args.substring in x
    if not args.diff:
        main(args.filename[0], decoder=args.dictionary,namelist=args.namelist,filter=filter)
    else:
        diff(args.filename, decoder=args.dictionary,namelist=args.namelist,filter=filter)

