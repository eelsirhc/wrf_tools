#!/usr/bin/env python
from netCDF4 import *
import matplotlib as mpl
mpl.use("Agg")
from pylab import *
from pydwrf import wrf
import sys
import seaborn as sns

def plot_mean(nc):
    sltime=(slice(None))

    def combine(area,data,keys,filt,fily,av=False):
        s=0.
        for k in keys:

            s+=sum(area[fily]*data[k]["value"][filt,fily]*data[k]["scale"],axis=1)
        if av:
            s=s/sum(area[fily])
        return s

    sxlat=slice(None)
    plot_core(nc,sxlat,combine,"L_s")

def plot_wrfout(nc):
    sltime=(slice(None))

    def combine(area,data,keys,filt,fily,av=False):
        s=0.
        for k in keys:
#            print area[fily].shape, data[k]["value"][filt,fily].shape
            s+=mean(sum(area[fily][None,:,None]*data[k]["value"][filt,fily]*data[k]["scale"],axis=1),-1)
        if av:
            s=s/sum(area[fily])
        return s

    sxlat=(0,slice(None),0)
    plot_core(nc,sxlat,combine,"L_S")
    
def plot_core(nc,sxlat,combine,lsvar):
    #nc = Dataset
    print nc["XLAT"].shape, sxlat
    xlat = nc["XLAT"][sxlat]
    area=(2*pi*cos(deg2rad(xlat))*deg2rad(180/36.))*3371000.**2
    
    #vapor mass in each hemisphere
    ls=wrf.variables.continuous_ls(nc[lsvar][:])
    sh=xlat[:]<0
    nh=xlat[:]>=0
    figure(figsize=(12,15))
    x=[]
    xl=(0,ceil(max(ls/360.)))
    sl=slice(None)#where((ls/360>xl[0])&(ls/360<xl[1]))
    ls=ls[sl]
    def zones(x,area, data, keys, filt, filys,plotfunc=None,av=False,*args,**kwargs):
        plotfunc=plotfunc or plot
        for fily_label,fily in filys.items():
            plotfunc(x,combine(area,data,keys,filt,fily,av=av),label=fily_label,*args,**kwargs)
    def dplot(x,y,*args,**kwargs):
        #    print y-y[1]
        return plot(x,y-y[1],*args,**kwargs)
    
    data = dict(vapor=dict(value=nc["QV_COLUMN"],scale=1./1e14),
                ice=dict(value=nc["QI_COLUMN"],scale=1./1e14),
                surface=dict(value=nc["H2OICE"],scale=1e3/1e14),
                taudust=dict(value=nc["TAU_OD2D"],scale=1),
                tauice=dict(value=nc["TAU_CL2D"],scale=1),
                )
    subplots_adjust(right=0.8)
    subplot(611)
    plot(ls/360,combine(area, data,["vapor"],sl,sh),label="SH vapor")
    plot(ls/360,combine(area, data,["vapor"],sl,nh),label="NH vapor")
    xlim(xl)
    ylim(0,40)
    legend(loc=(1.1,0.25))
    subplot(612)
    zones(ls/360.,area,data,["vapor","ice","surface"],sl,{"delta SH water":sh,"delta NH water":nh},plotfunc=dplot)
    zones(ls/360.,area,data,["vapor","ice"],sl,{"delta SH atmwater":sh,"delta NH atmwater":nh},plotfunc=dplot,ls='--')
    
    legend(loc=(1.1,0.25))
    xlim(xl)
    ylim(-75,75)
    subplot(613)
    sh30=xlat<-30
    eq=(xlat>=-30)&(xlat<30)
    nh30=xlat>=30
    zones(ls/360.,area,data,["ice"],sl,{"SH<30 Ice":sh30,"eq Ice":eq,"NH>30 Ice":nh30})
    legend(loc=(1.1,0.25))
    xlim(xl)
    ylim(0,5)
    subplot(614)
    nh75=xlat>75
    sh85=xlat<-85
    eq=~nh75&~sh85
    dplot(ls/360,combine(area,data,["ice","vapor","surface"],sl,nh75),label="NH >75 water")
    dplot(ls/360,combine(area,data,["surface"],sl,eq) + combine(area,data,["ice","vapor"],sl,~nh75),label="NH < 75 atm water")
    dplot(ls/360,combine(area,data,["surface"],sl,sh85),label="SH <-85 surface water")
    #dplot(ls/360,sum(area[nh75] * (nc["QI_COLUMN"][:,nh75]+nc["QV_COLUMN"][:,nh75])/1e14,axis=1),label="NH >75 atm water",ls='--')
    legend(loc=(1.1,0.25))
    ylim(-100,100)
    twinx()
    dplot(ls/360,combine(area,data,["ice","vapor","surface"],sl,xlat>-90),label="tots. water",ls='--',color='orange')
    dplot(ls/360,combine(area,data,["ice","vapor"],sl,nh75),label="NH >75 atm water",ls='--',color='magenta')
    legend(loc=(1.1,0.0))
    xlim(xl)
    ylim(-10,10)
    subplot(615)
    sh30=xlat<-30
    eq=(xlat>=-30)&(xlat<30)
    nh30=xlat>=30
    zones(ls/360.,area,data,["taudust"],sl,{"SH<30 taudust":sh30,"eq taudust":eq,"NH>30 taudust":nh30},av=True)
    legend(loc=(1.1,0.5))
    xlim(xl)
    ylim(0,3)
    subplot(616)
    zones(ls/360.,area,data,["tauice"],sl,{"SH<30 tauice":sh30,"eq tauice":eq,"NH>30 tauice":nh30},av=True)
    legend(loc=(1.1,0.5))
    xlim(xl)
    ylim(0,10)
    savefig("water.png")


if __name__ == "__main__":
    nc = Dataset(sys.argv[1])
    if "west_east" in nc["XLAT"].dimensions:
        nc.close()
        nc=MFDataset(sys.argv[1:])
        plot_wrfout(nc)
    else:
        plot_mean(nc)
    
    
