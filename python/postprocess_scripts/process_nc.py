#!/usr/bin/env python
#Requires the pydwrf package and pydwell package for command line support
#git@bitbucket.org:eelsirhc/pydwrf.git
#git@bitbucket.org:eelsirhc/pydwell.git

import pydwrf.wrf as wrf
import netCDF4
import numpy
import dwell.rad.terminal as terminal
from argh import arg

#who really knows what this code does.

def process_dimensions(nc):
    """Process all of the available dimensions"""
    indices=0
    aliases=dict()
    dimensions=dict()
    
    def add_dimension(nc,varname, indices,outname,aliases,dimensions,sel=None):
        """Add a single dimension, isn't this also below"""
        sel = sel or slice(None,None,None)
        if varname in nc.variables:
            #vertical
            var = wrf.variables.get(nc,varname, indices=indices)[sel]
            
            aliases[varname]=outname
            dimensions[outname]=var
        elif varname=="ZNU":
            var = numpy.arange(52)
            aliases[varname]=outname
            dimensions[outname]=var
        return aliases, dimensions

    all_aliases =dict(XLAT="south_north",
                 XLAT_V="south_north_stag",
                 XLONG="west_east",
                 XLONG_U="west_east_stag",
                 ZNU="bottom_top",
                 ZNW="bottom_top_stag",
                 ZS="soil_layers_stag",
                 DZS="soil_layers_thickness")
                 
    sel=dict(XLAT=[slice(None,None,None),0],
                 XLAT_V=[slice(None,None,None),0],
                 XLONG=[0,slice(None,None,None)],
                 XLONG_U=[0,slice(None,None,None)],
                 ZNU=None,
                 ZNW=None,
                 ZS=None,
                 DZS=None)
    
    for varname in ["ZNU","ZNW","ZS","XLAT","XLAT_V","XLONG","XLONG_U","DZS"]:
        aliases,dimensions = add_dimension(nc,varname, indices, all_aliases[varname], aliases, dimensions, sel[varname])

    return dimensions,aliases

def mean_over(vdata, vdim, indices, over):
    """Figure out which axes to average over, account for _staggered axis.
    Return the mean variable"""
    zm,rm=[],[]
    fix=lambda s: s.replace("_stag","")
    delta = set([fix(d) for d in vdim]).difference(set(over))
    avindex = set([fix(d) for d in vdim]).intersection(set(over))
    mapping = dict([(fix(j),i) for i,j in enumerate(vdim)])
    rm = [r for r in vdim if fix(r) in delta]#sorted(apping[o] for o in delta)
    zm=[]
    for o in avindex:
        if o == "Time" and not isinstance(indices,int):
            zm.append(mapping[o])
        elif o != "Time" and not isinstance(indices,int):
            zm.append(mapping[o])
        elif o != "Time" and isinstance(indices,int):
            zm.append(mapping[o]-1)

    zm = sorted(zm)
    return wrf.analysis.mean(vdata,zm), rm

def single(nc,indices,over,vname):
    """Process a single named variable witha a generic mean function"""
    data=dict()
    dimensions=dict()
    attributes=dict()
    if vname in nc.variables:
        vdata,vdim = wrf.variables.get(nc,vname,indices=indices,return_dimensions=True)
        data[vname],dimensions[vname]=mean_over(vdata,vdim,indices,over=over)
        del vdata

    return data,dimensions,attributes

def process_ampm(nc,indices,over):
    """Process the variables from the TES aux9 file"""
    print ("Getting AMPM")
    data=dict()
    dimensions=dict()
    attributes=dict()
    for vname in ["PB_AM","PB_PM","P_AM","P_PM","PSFC_AM","PSFC_PM","TSK_AM","TSK_PM","T_PHY_AM","T_PHY_PM","U_AM","U_PM","V_AM","V_PM","TAU_OD2D_AM","TAU_CL2D_AM","TAU_OD2D_PM","TAU_CL2D_PM","QV_COLUMN_AM","QI_COLUMN_AM","QV_COLUMN_PM","QI_COLUMN_PM"]:
        da,di,at = single(nc,indices,over,vname)
        data.update(da)
        dimensions.update(di)
        attributes.update(at)
            
    return data,dimensions,attributes

def process_tracers(nc,indices,over):
    """Process the tracers used by the water cycle"""
    print ("Getting tracers")
    data=dict()
    dimensions=dict()
    attributes=dict()
    for vname in ["TRC01","TRC02", "REFF_DUST","TAU_OD","QICE","QNICE","QVAPOR", "TRC_IC","REFF_ICE",
                  "DUSTNC","DUSTNCV","CORENC","CORENCV","DUSTQ_MICRO","DUSTQ_SED",
                  "DUSTN_MICRO","DUSTN_SED","COREQ_MICRO","COREQ_SED",
                  "ICEQ_MICRO","ICEQ_SED","ICEN_MICRO","ICEN_SED"
                  ]:
        da,di,at = single(nc,indices,over,vname)
        data.update(da)
        dimensions.update(di)
        attributes.update(at)
            
    return data,dimensions,attributes
    
def process_Uwind(nc,indices,over):
    """Process the U component of wind"""
    print("Getting U wind only")

    data=dict()
    dimensions=dict()
    attributes=dict()
    vname="U"
    da,di,at = single(nc,indices,over,vname)
    data.update(da)
    dimensions.update(di)
    attributes.update(at)
    return data,dimensions,attributes

def process_winds(nc,indices,over):
    """Process the three components of wind"""
    print("Getting winds")

    data=dict()
    dimensions=dict()
    attributes=dict()
    for vname in ["U","V","W"]:
        da,di,at = single(nc,indices,over,vname)
        data.update(da)
        dimensions.update(di)
        attributes.update(at)

    return data,dimensions,attributes

def process_temperature(nc,indices,over):
    """Process theta and pressure to get temperature"""
    
    print ("Getting Temperature")
    p0,t0,rd,cp = nc.P0,nc.T0,nc.R_D,nc.CP
    data=dict()
    dimensions=dict()
    attributes=dict()    

    if "T" in nc.variables and "P" in nc.variables and "PB" in nc.variables:
        T,Tdim = wrf.variables.get(nc,"T",indices=indices,return_dimensions=True)
        p,pdim = wrf.variables.get(nc,"P",indices=indices,return_dimensions=True)
        pb = wrf.variables.get(nc,"PB",indices=indices)
        
        kappa=rd/cp
        press=p+pb
        exner = press/p0
        temp = (exner**kappa) * (T+t0)

        del p,pb,exner

        data["T"],dimensions["T"]         = mean_over(temp , Tdim, indices, over=over)
        data["Theta"],dimensions["Theta"] = mean_over(T+t0 , pdim, indices, over=over)
        data["P"],dimensions["P"]         = mean_over(press, pdim, indices, over=over)
    
        del T,temp,press

    return data,dimensions,attributes

def process_subsurface_ice(nc, indices, over):
    """Convert the ice fields into a 2D ice map"""
    data=dict()
    dimensions=dict()
    attributes=dict()

    if "SS_ICE" in nc.variables and "SS_VAPOR" in nc.variables and "SS_ADSORBATE" in nc.variables and "POROSITY_MOD" in nc.variables:
        si, sidim = wrf.variables.get(nc,"SS_ICE",indices=indices, return_dimensions=True)
        sv, svdim = wrf.variables.get(nc,"SS_VAPOR",indices=indices, return_dimensions=True)
        sa, sadim = wrf.variables.get(nc,"SS_ADSORBATE",indices=indices, return_dimensions=True)
        sp, spdim = wrf.variables.get(nc,"POROSITY_MOD",indices=indices, return_dimensions=True)

        dzs = nc["DZS"][indices,:]
        def strip_soil(d):
            d = list(d)#
            d.remove('soil_layers_stag')
            return d

        s=slice(None,None,None)
        icemass, dimi = mean_over(sum(si[s]*dzs[s,None,None],0), strip_soil(sidim), indices, over=over)
        del si
        vapormass, dimv = mean_over(sum(sp[s]*sv[s]*dzs[s,None,None],0), strip_soil(svdim), indices, over=over)
        vapormassnp, dimvnp = mean_over(sum(sv[s]*dzs[s,None,None],0), strip_soil(svdim), indices, over=over)
        del sv,sp
        adsmass, dima = mean_over(sum(sa[s]*dzs[s,None,None],0), strip_soil(sadim), indices, over=over) 
        del sa

        data["subsurface_icemass"]=icemass+vapormass+adsmass
        dimensions["subsurface_icemass"] = dimi

        data["ssice"]=icemass
        dimensions["ssice"]=dimi

        data["ssvapor"]=vapormass
        dimensions["ssvapor"]=dimv

        data["ssvapornp"]=vapormassnp
        dimensions["ssvapornp"]=dimvnp

        data["ssadsorbate"]=adsmass
        dimensions["ssadsorbate"]=dima
        
    return data, dimensions, attributes


def process_subsurface(nc, indices, over):
    """Process the subsurface variables with their special subsurface vertical coordinate"""
    subsurface_vars = ["TSLB", "SS_ICE", "SS_VAPOR","SS_ADSORBATE",
                       "POROSITY","POROSITY_MOD","SOIL_DEN","SOIL_CAP","SOIL_COND"]

    print ("Getting Subsurface variables")
    a,b,c = internal_process_surface(nc,indices, subsurface_vars, over)
    a2,b2,c2 = process_subsurface_ice(nc,indices, over)
    a.update(a2)
    b.update(b2)
    c.update(c2)

    return a,b,c

def process_surface_all(nc,indices,over):
    """Process a larger set of surface variables, defined as 'all' of them"""
    surface_vars = ["MU","MUB","PSFC","T1_5","TH1_5",\
                    "U1_5","V1_5","U1M","V1M","RHOS",\
                    "QFLUX","HGT","NLIF1","DLIF1","DSED1",\
                    "SURFTRACER1","TSK","SWDOWN","SWDOWNDIR",\
                    "GSWSLOPE","GSW","GLW","SWNORM","TOASW",\
                    "TOALW","ALBEDO","ALBBCK","EMBCK","THCBCK",\
                    "EMISS","PBLH","THC","HFX","RNET_2D","LH",\
                    "H2OICE","CO2ICE","TAU_OD2D","TAU_CL2D",\
                    "GRD_ICE_PC","GRD_ICE_DP","QV_COLUMN","QI_COLUMN","CDOD_SCALE","DLIF_EXTRA"]
    print ("Getting All Surface variables")
    return internal_process_surface(nc,indices,surface_vars,over)

def process_surface(nc,indices,over):
    """Process the common surface and 2D variables, temperature pressure, fluxes, opacities"""
    surface_vars = ["MU","MUB","PSFC","TSK",\
                    "H2OICE","CO2ICE",\
                    "GSW","GLW","TOASW","TOALW","HFX","RNET_2D",\
                        "ALBEDO","EMISS","TAU_OD2D","CDOD_SCALE"]
    print ("Getting Surface variables")
    return internal_process_surface(nc,indices,surface_vars,over)

def process_aux5(nc,indices,over):
    """Process the three variables in the aux5 file, or simulate a zonal mean aux5 file I guess
    PSFC,HGT,TSK"""

    surface_vars = ["PSFC","HGT","TSK"]
    print ("Getting aux5 Surface variables")
    return internal_process_surface(nc,indices,surface_vars,over)


def internal_process_surface(nc,indices,surface_vars,over):
    """Internal function, fiven a surface variable, calculate it's mean and return it"""
    def surface_mean(nc,varname):
        var,vardim = wrf.variables.get(nc,varname,indices=indices,return_dimensions=True)
        data,dim = mean_over(var, vardim, indices, over=over)
        del var
        return data, dim, dict()

    data=dict()
    dimensions=dict()
    attributes=dict()    

    for vname in surface_vars:
        da,di,at = single(nc,indices,over,vname)
        data.update(da)
        dimensions.update(di)
        attributes.update(at)

    return data, dimensions,attributes

def process_indices(nc,indices,over,process_list=None):
    """For a list of indices and process_list, run the each function in the
    process_list over the indices"""

    data = dict()
    dimensions = dict()
    attributes = dict()
    if process_list is None:
        process_list=["winds","temperature","surface","tracers"]
    for funcname in process_list:
        full_funcname = "process_{0}".format(funcname)
        if full_funcname not in globals():
            print("Cannot find function {0}".format(full_funcname))
            continue
        function = globals()[full_funcname]
        mydata,mydimensions,myattributes = function(nc,indices,over)
        data.update(mydata)
        dimensions.update(mydimensions)
        attributes.update(myattributes)
    return data,dimensions,attributes

def initialize_netcdf(filename,clobber=False):
    """Create the netcdf file, optionally delete the old one"""
    nc = netCDF4.Dataset(filename,'w',clobber=clobber,format='NETCDF4')
    return nc

def createDimensions(root,dimensions):
    """Creates a dimension and defines its length"""
    for d,v in dimensions.items():
        root.createDimension(d,v)
    return root

def createCoordinates(root,dimensions,aliases=None):
    """Creates a coordinate variable. A variablle with the same name and shape as a dimension"""
    createDimensions(root,dict([c,len(v)] for c,v in dimensions.items()))
    if aliases:
        for c,v in aliases.items():
            root.createVariable(c,dimensions[v].dtype,(v,))
            root.variables[c][:]=dimensions[v]
    else:
        for c,v in dimension.items():
            cval = root.createVariable(c,v.dtype,(c,))
            root.variables[c][:]=v
    return root

def createLsDimension(root,lslow,lshigh,lsname="L_s"):
    """Create the renamed L_s dimension, yes, that was wrong of me"""
    root.createDimension(lsname,len(lslow))
    root.createVariable(lsname,lslow.dtype,(lsname,))
    root.variables[lsname][:] = lslow
    
    
def createVariables(root,variables,dimensions,attributes=None,lsname="L_s"):
    """Creates a variable in a netcdf4 file, with known dimensions and attributes"""
    for k,v in variables.items():
        print ("Creating {0}".format(k))
        dims = list(dimensions[k])
        dims.insert(0,lsname)
        root.createVariable(k,v.dtype,dims)
        root.variables[k][:] = v
        if attributes is not None:
            if k in attributes:
                root.variables[k].setncatts(attributes[k])
    
        
def process_seasons(nc,year, seasons=None,season_delta=30,process_list=None,output=None,clobber=False,over=None):
    """Calculates the seasonal means.
        1. generates a list of seasonal ranges
        2. filters on those ranges
        3. iterates through the ranges to calculate the mean data
    """
    over = over or ["Time","west_east"]
    if seasons is None:
        seasons=numpy.arange(360/season_delta)
        #default is all 12 = 360/30
    lsrange = [[s*season_delta,s*season_delta+season_delta] for s in seasons]
    indices = [wrf.filter_indices(nc,year=[year,year],ls=l) for l in lsrange]

    print("Getting Dimensions")
    dimensions,aliases = process_dimensions(nc)
    if output is None:
        output = "out.nc"
    ncout=initialize_netcdf(output,clobber=clobber)

    createCoordinates(ncout,dimensions,aliases=aliases)
    createLsDimension(ncout,numpy.array(seasons)*season_delta,(numpy.array(seasons)+1)*season_delta)
    
    data=dict()
    for season,index,lsval in zip(seasons,indices,lsrange):
        print ("Season {0}, Ls {1}-{2}".format(season,lsval[0],lsval[1]))
        data[season],dimensions,attributes = process_indices(nc,index,over,process_list=process_list)

    #merge dictionary entries
    output=dict()
    for key in data[seasons[0]]:
        print key
        output[key] = numpy.concatenate([data[s][key][numpy.newaxis,:] for s in seasons])
    del data

    #save_netcdf(data,dimensions,aliases)
    createVariables(ncout,output,dimensions,attributes)
    ncout.close()

def process_mean(nc, clobber=False, output=None, process_list=None,over=None):
    """Calculates the zonalmean file"""
    over = over or ["Time","west_east"]
    ls = nc.variables["L_S"][:]
    indices=numpy.arange(len(ls))
    seasons=indices
    
    print("Getting Dimensions")
    dimensions,aliases = process_dimensions(nc)
    if output is None:
        output = "out.nc"
    ncout=initialize_netcdf(output,clobber=clobber)

    createCoordinates(ncout,dimensions,aliases=aliases)
    createLsDimension(ncout,ls,ls)
    
    data=dict()
    for season,index,lsval in zip(seasons,indices,ls):
        print ("Season {0}, Ls {1}".format(season,lsval))
        data[season],dimensions,attributes = process_indices(nc,index,process_list=process_list,over=over)

    #merge dictionary entries
    output=dict()
    for key in data[seasons[0]]:
        output[key] = numpy.concatenate([data[s][key][numpy.newaxis,:] for s in seasons])
    del data

    #save_netcdf(data,dimensions,aliases)
    createVariables(ncout,output,dimensions,attributes)
    ncout.close()

    
def csv_seasons(s):
    """Knows above seasonal ranges, given a list of csv ranges A-B,C,D,E-F produces a list of tuples or ints
    A,A+1,A+2,A+3,B-1,C,D,E,E+1,E+2..F-1.
    The expected format of the seasons is 0-Nseasons, not 0-360. 
    e.g asking for 0-2 with delta=30 gives you 0-30,30-60,60-90,
     with delta=1 gives you 0-1,1-2,2-3
    """
    r=[]
    for x in s.split(','):
        print x
        if x.count("-"):

            r.extend(range(int(x.split("-")[0]),int(x.split("-")[1])))
        else:
            r.append(int(x))
    return r
 
def parse_csv_gen(t):
    """generates a csv list generator function of type t"""
    def parse_csv(s):
        """splits a comma separated list into a list of type t"""
        return [t(x) for x in s.split(",")]
    return parse_csv

@arg("filenames",nargs="+")
@arg("--seasons","-s",type=csv_seasons)
@arg("--process","-p",type=parse_csv_gen(str))
@arg("--year",type=int)
@arg("--over",type=parse_csv_gen(str))
def process(filenames,year=None,seasons=None,process=None,delta=30,output=None,clobber=False,over=None):
    """Bins into seasons of delta Ls long. Zonal and time averages"""
    over=over or ["Time","west_east"]
    nc = netCDF4.MFDataset(filenames,aggdim="Time")
    if year is None:
        year=2
    process_seasons(nc,year,season_delta=delta,process_list=process,output=output,clobber=clobber,seasons=seasons,over=over)

@arg("filenames",nargs="+")
@arg("--process","-p",type=parse_csv_gen(str))
@arg("--over",type=parse_csv_gen(str))
def zonalmean(filenames,clobber=False,output=None, process=None,over=None):
    """ Calculates zonalmean data for every timestep by averaging over the west_east dimension. (and _stag dimension)"""
    over=over or ["Time","west_east"]
    nc = netCDF4.MFDataset(filenames,aggdim="Time")
    process_mean(nc, clobber=clobber,output=output,process_list=process,over=over)


if __name__=="__main__":
    terminal.argh_main("process_nc",[process,zonalmean])

        
    
