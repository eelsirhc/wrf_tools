#!/usr/bin/env python
from plot_common import *
import os
def plot_core(nc,sxlat,combine,lsvar):
    #nc = Dataset
    xlat = nc["XLAT"][sxlat]
    area=(2*pi*cos(deg2rad(xlat))*deg2rad(180/36.))*3371000.**2
    
    #vapor mass in each hemisphere
    ls=wrf.variables.continuous_ls(nc[lsvar][:])
    figure(figsize=(12,15))
    x=[]
    xl=(0,ceil(max(ls/360.)))
    sl=slice(None)#where((ls/360>xl[0])&(ls/360<xl[1]))
    ls=ls[sl]
    
    result = dict()
    data = dict(vapor=dict(value=nc["QV_COLUMN"],scale=1.),
                ice=dict(value=nc["QI_COLUMN"],scale=1.),
                surface=dict(value=nc["H2OICE"],scale=1e3),
                taudust=dict(value=nc["TAU_OD2D"],scale=1),
                tauice=dict(value=nc["TAU_CL2D"],scale=1),
                tsk=dict(value=nc["TSK"],scale=1),
                co2ice=dict(value=nc["CO2ICE"],scale=1./1e15),
                t29=dict(value=nc["T"][:,28]+(50.-nc["P"][:,28])*(nc["T"][:,29]-nc["T"][:,28])/(nc["P"][:,29]-nc["P"][:,28]),scale=1.),
                #p29=dict(value=nc["P"][:,29],scale=1.),

                )
#    s=18
#    l=40
#    print "t28: ", nc["T"][l,28,s]
#    print "p50", (50.-nc["P"][l,28,s])
#    print "m",(nc["T"][l,29,s]-nc["T"][l,28,s])/(nc["P"][l,29,s]-nc["P"][l,28,s])
    sh=xlat[:]<-80
    nh=xlat[:]>+80
    
    subplots_adjust(right=0.8)
    subplot(611)
    zones(ls/360.,area,data,["vapor"],sl,{"SH water":sh,"NH water":nh},combine,av=True,result=result, figure=1)
    zones(ls/360.,area,data,["ice"],sl,{"SH ice":sh,"NH ice":nh},combine,av=True,result=result, figure=1)
    #zones(ls/360.,area,data,["surface"],sl,{"delta SH surface":sh,"delta NH surface":nh},combine,av=True, plotfunc=dplot)
    xlim(xl)
    ylabel("Prum")
    ylim(0,60)
    legend(loc=(1.1,0.25))
    title(os.getcwd())
    subplot(612)
    zones(ls/360.,area,data,["tauice"],sl,{"SH tauice":sh,"NH tauice":nh},combine,av=True,result=result, figure=2)
    zones(ls/360.,area,data,["taudust"],sl,{"SH taudust":sh,"NH taudust":nh},combine,av=True,result=result, figure=2)
    xlim(xl)
    ylabel("tau")
    legend(loc=(1.1,0.25))
    
    subplot(613)
    zones(ls/360.,area,data,["tsk"],sl,{"SH TSK":sh,"NH TSK":nh},combine,av=True,result=result, figure=3)
    xlim(xl)
    ylabel("T (K)")
    legend(loc=(1.1,0.25))
    
    eq=(xlat[:]>-15)&(xlat[:]<15)
    NHtropics=(xlat[:]>15)&(xlat[:]<45)
    SHtropics=(xlat[:]>-45)&(xlat[:]<-15)
    NHextra=xlat[:]>45
    SHextra=xlat[:]<-45
    subplot(614)
    zones(ls/360.,area,data,["vapor"],sl,{  "SH 45-90 water":SHextra,
                                            "SH 15-45 water":SHtropics,
                                            "EQ water":eq,
                                            "NH 15-45 water":NHtropics,
                                            "NH 45-90 water":NHextra},combine,av=True,
          result=result, figure=4)
    xlim(xl)
    ylabel("prum")
    legend(loc=(1.1,0.25))
    
    subplot(615)
    zones(ls/360.,area,data,["ice"],sl,{  "SH 45-90 ice":SHextra,
                                            "SH 15-45 ice":SHtropics,
                                            "EQ ice":eq,
                                            "NH 15-45 ice":NHtropics,
                                            "NH 45-90 ice":NHextra},combine,av=True,
          result=result, figure=5)
    xlim(xl)
    ylabel("prum")
    legend(loc=(1.1,0.25))
    
    eq5 = (xlat[:]<5)&(xlat[:]>-5)
    subplot(616)
    zones(ls/360.,area,data,["t29"],sl,{"Eq T0.5hPa":eq5},combine,av=True,result=result, figure=6)
    xlim(xl)
    #twinx()
    #plot(ls/360.,area,data,["p29"],sl,{"Eq P29":eq5},combine,av=True,color='red')
    ylabel("T (K)")
    legend(loc=(1.1,0.25))
        

    savefig("temp.png")
    return result

if __name__ == "__main__":
    nc = Dataset(sys.argv[1])
    if "west_east" in nc["XLAT"].dimensions:
        nc.close()
        nc=MFDataset(sys.argv[1:])
        result = plot_wrfout(nc,plot_core)
    else:
        result = plot_mean(nc,plot_core)
    
    save_result(result,"temp.txt")
