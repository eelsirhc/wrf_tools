#!/usr/bin/env python
from plot_common import *

def plot_core(nc,sxlat,combine,lsvar):
    #nc = Dataset
    print nc["XLAT"].shape, sxlat
    xlat = nc["XLAT"][sxlat]
    area=(2*pi*cos(deg2rad(xlat))*deg2rad(180/36.))*3371000.**2
    
    #vapor mass in each hemisphere
    ls=wrf.variables.continuous_ls(nc[lsvar][:])
    sh=xlat[:]<0
    nh=xlat[:]>=0
    figure(figsize=(12,15))
    x=[]
    xl=(0,ceil(max(ls/360.)))
    sl=slice(None)#where((ls/360>xl[0])&(ls/360<xl[1]))
    ls=ls[sl]
    
    result = dict()
    
    class zeroclass(object):
        def __init__(self,template):
            self.template = template
        def __getitem__(self,a):
            return self.template[a]*0.

    data = dict(vapor=dict(value=nc["QV_COLUMN"],scale=1./1e14),
                ice=dict(value=nc["QI_COLUMN"],scale=1./1e14),
                surface=dict(value=nc["H2OICE"],scale=1e3/1e14),
                taudust=dict(value=nc["TAU_OD2D"],scale=1),
                tauice=dict(value=nc["TAU_CL2D"],scale=1),
                subsurface=dict(value=zeroclass(nc["QI_COLUMN"]),scale=1/1e14)
                )

    if "subsurface_icemass" in nc.variables:
        print "cat"
        data["subsurface"]=dict(value=nc["subsurface_icemass"],scale=1e3/1e14)

    subplots_adjust(right=0.8)
    subplot(611)
    aplot(ls/360,combine(area, data,["vapor"],sl,sh),label="SH vapor",result=result, figure=1)
    aplot(ls/360,combine(area, data,["vapor"],sl,nh),label="NH vapor",result=result, figure=1)
    xlim(xl)
    ylim(0,40)
    legend(loc=(1.1,0.25))
    subplot(612)
    zones(ls/360.,area,data,["vapor","ice","surface","subsurface"],sl,{"delta SH water":sh,"delta NH water":nh},combine,plotfunc=dplot, result=result,figure=2)
    zones(ls/360.,area,data,["vapor","ice"],sl,{"delta SH atmwater":sh,"delta NH atmwater":nh},combine,plotfunc=dplot,ls='--',result=result,figure=2)
    
    legend(loc=(1.1,0.25))
    xlim(xl)
    ylim(-75,75)
    subplot(613)
    sh30=xlat<-30
    eq=(xlat>=-30)&(xlat<30)
    nh30=xlat>=30
    zones(ls/360.,area,data,["ice"],sl,{"SH<30 Ice":sh30,"eq Ice":eq,"NH>30 Ice":nh30},combine,result=result,figure=3)
    legend(loc=(1.1,0.25))
    xlim(xl)
    ylim(0,5)
    subplot(614)
    nh75=xlat>75
    sh85=xlat<-85
    eq=~nh75&~sh85
#    result[4]["NH >75 surface"]=(ls/360,combine(area,data,["surface"],sl,nh75))
    dplot(ls/360,combine(area,data,["ice","vapor","surface","subsurface"],sl,nh75),label="NH >75 water",result=result,figure=4)
    dplot(ls/360,combine(area,data,["surface","subsurface"],sl,eq) + combine(area,data,["ice","vapor"],sl,~nh75),label="NH < 75 atm water",result=result,figure=4)
    dplot(ls/360,combine(area,data,["surface","subsurface"],sl,sh85),label="SH <-85 surface water",result=result,figure=4)
    #dplot(ls/360,sum(area[nh75] * (nc["QI_COLUMN"][:,nh75]+nc["QV_COLUMN"][:,nh75])/1e14,axis=1),label="NH >75 atm water",ls='--')
    legend(loc=(1.1,0.25))
    ylim(-100,100)
    twinx()
    dplot(ls/360,combine(area,data,["ice","vapor","surface","subsurface"],sl,xlat>-90),label="tots. water",ls='--',color='orange',result=result,figure=5)
    dplot(ls/360,combine(area,data,["ice","vapor"],sl,nh75),label="NH >75 atm water",ls='--',color='magenta',result=result,figure=5)
    legend(loc=(1.1,0.0))
    xlim(xl)
    ylim(-100,100)
    subplot(615)
    sh30=xlat<-30
    eq=(xlat>=-30)&(xlat<30)
    nh30=xlat>=30
    zones(ls/360.,area,data,["taudust"],sl,{"SH<30 taudust":sh30,"eq taudust":eq,"NH>30 taudust":nh30},combine,av=True,result=result,figure=6)
    legend(loc=(1.1,0.5))
    xlim(xl)
    ylim(0,3)
    subplot(616)
    zones(ls/360.,area,data,["tauice"],sl,{"SH<30 tauice":sh30,"eq tauice":eq,"NH>30 tauice":nh30},combine,av=True,result=result,figure=7)
    legend(loc=(1.1,0.5))
    xlim(xl)
    ylim(0,10)
    savefig("water.png")

    return result
if __name__ == "__main__":
    nc = Dataset(sys.argv[1])
    if "west_east" in nc["XLAT"].dimensions:
        nc.close()
        nc=MFDataset(sys.argv[1:])
        result = plot_wrfout(nc,plot_core)
    else:
        result = plot_mean(nc,plot_core)


    save_result(result,"water.txt")
